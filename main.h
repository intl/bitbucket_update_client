#ifndef __MAIN_H__
#define __MAIN_H__

#include <windows.h>
#include <git2.h>
#include <commctrl.h>
#include <commdlg.h>
#include <curl/curl.h>
#include <string.h>
#include <json/json.h>
#include <sstream>
#include <fstream>
#include <sys/stat.h>
#include <stdlib.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>



#define GIT_REPOSITORY_USER intl
#define GIT_REPOSITORY_NAME intl.bitbucket.org
#define VER_FILE_NAME "version"
#define GIT_REPOSITORY_URL "https://bitbucket.org/GIT_REPOSITORY_USER/GIT_REPOSITORY_NAME.git"
#define PRJ_BIN_FOLDER "bin"
#define GIT_BITBUCKET_API_CHANGESET_URL "https://api.bitbucket.org/1.0/repositories/intl/intl.bitbucket.org/changesets?limit=1"

#define UPDATE_NEEDED 0
#define UPDATE_ERROR 1
#define UPDATE_NOT_NEEDED 2

#define UPDATE_SUCCESSFUL 0

#define SKIP_HOSTNAME_VERIFICATION
#define SKIP_PEER_VERIFICATION

#define _WIN32_IE 0x0500
#define UNICODE
#define _UNICODE

/*  To use this exported function of dll, include this header
 *  in your project.
 */

#ifdef BUILD_DLL
    #define DLL_EXPORT __declspec(dllexport)
#else
    #define DLL_EXPORT __declspec(dllimport)
#endif


#ifdef __cplusplus
extern "C"
{
#endif

char DLL_EXPORT make_clone();
void DLL_EXPORT make_window();

DLL_EXPORT int make_update_force();
DLL_EXPORT int make_update_auto();
DLL_EXPORT int make_update_standalone();

unsigned int DLL_EXPORT is_update_needed();
#ifdef __cplusplus
}
#endif

#endif // __MAIN_H__

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;


namespace update_client
{

class update_class
    {
        [DllImport("..\\upd_cl_cpp.dll")]
        public extern static uint is_update_needed();
        public const uint UPDATE_NEEDED = 0;
        public const uint UPDATE_ERROR = 1;
        public const uint UPDATE_NOT_NEEDED = 2;

        public static void make_update()
        {
            Process.Start("rundll32.exe", "..\\upd_cl_cpp.dll,make_update_force");
            Environment.Exit(0);
        }

        public static void check_and_update()
        {
            if (is_update_needed() == UPDATE_NEEDED)
            {
                make_update();
            }
        }
    }
}
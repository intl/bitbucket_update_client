#include "main.h"

using namespace std;

static HWND hwnd_progress_bar;
static HWND hwnd_window;

typedef struct progress_data {
	git_transfer_progress fetch_progress;
	size_t completed_steps;
	size_t total_steps;
	const char *path;
} progress_data;

struct MemoryStruct {
  char *memory;
  size_t size;
};

/*
UPDATE_AUTO - auto update with no additional messages
UPDATE_STANDALONE - standalone update, with additional debug/error messages
UPDATE_FORCE - force cloning git repository
*/

enum update_params {UPDATE_AUTO,UPDATE_STANDALONE,UPDATE_FORCE};

EXTERN_C IMAGE_DOS_HEADER __ImageBase;

string get_path_to_rep()
{
    char buffer[MAX_PATH];
    GetModuleFileName( (HINSTANCE)&__ImageBase, buffer, MAX_PATH );

    string::size_type pos = string( buffer ).find_last_of( "\\/" );
    return string( buffer ).substr( 0, pos);
}

static size_t get_version_file_cb(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;
  mem->memory = (char*)realloc((char*)(mem->memory), mem->size + realsize + 1);
  if(mem->memory == NULL)
    {
    /* out of memory! */
    MessageBox(NULL, TEXT("Error allocating memory."), TEXT("Error"), MB_ICONERROR | MB_OK);
    return 0;
    }
  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;
  return realsize;
}

static void print_progress(const progress_data *pd)
{
        int network_percent = (100*pd->fetch_progress.received_objects) / pd->fetch_progress.total_objects;
        SendMessageW(hwnd_progress_bar,0,PBM_SETRANGE,MAKEWPARAM(0,pd->fetch_progress.total_objects));
        SendMessage(hwnd_progress_bar, PBM_SETPOS, network_percent, 0);
}

static int fetch_progress(const git_transfer_progress *stats, void *payload)
{
	progress_data *pd = (progress_data*)payload;
	pd->fetch_progress = *stats;
	print_progress(pd);
	return 0;
}

static void checkout_progress(const char *path, size_t cur, size_t tot, void *payload)
{
	progress_data *pd = (progress_data*)payload;
	pd->completed_steps = cur;
	pd->total_steps = tot;
	pd->path = path;
	print_progress(pd);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg,WPARAM wParam, LPARAM lParam)
{
  switch(msg)
    {
    case WM_DESTROY:
      PostQuitMessage(0);
      return 0;
    }
  return DefWindowProcW(hwnd, msg, wParam, lParam);
}

void DLL_EXPORT make_window()
{
  INITCOMMONCONTROLSEX icc;
  WNDCLASSW wc;
  HINSTANCE hInstance;

  icc.dwSize = sizeof(icc);
  icc.dwICC = ICC_WIN95_CLASSES;
  InitCommonControlsEx(&icc);

  hInstance = GetModuleHandle(NULL);

  wc.style         = CS_HREDRAW | CS_VREDRAW ;
  wc.cbClsExtra    = 0;
  wc.cbWndExtra    = 0;
  wc.lpszClassName = L"window";
  wc.hInstance     = hInstance;
  wc.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
  wc.lpszMenuName  = NULL;
  wc.lpfnWndProc   = WndProc;
  wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
  wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);

  // Register our window classes, or error.
    if (!RegisterClassW(&wc))
        {
        MessageBox(NULL, TEXT("������ ����������� ���� RegisterClassW"), TEXT("������"), MB_ICONERROR | MB_OK);
        return ;
        }

  // Create instance of main window.
  hwnd_window = CreateWindowExW(WS_EX_TOOLWINDOW,wc.lpszClassName, L"Dinistor", WS_DLGFRAME|WS_TILEDWINDOW|WS_VISIBLE,
                100, 100, 145, 70, NULL, NULL, hInstance, NULL);

  // Error if window creation failed.
  if (!hwnd_window)
    {
    MessageBox(NULL, TEXT("������ �������� ��������� ����"), TEXT("������"), MB_ICONERROR | MB_OK);
    return ;
    }

   hwnd_progress_bar = CreateWindowExW(0,PROGRESS_CLASSW, L"progress_bar0",
                              WS_CHILD | WS_VISIBLE | WS_TABSTOP | PBS_SMOOTH,
                              10, 10, 110, 20,
                              hwnd_window, NULL,
                              (HINSTANCE) GetWindowLong (hwnd_window, GWL_HINSTANCE), NULL);

  ShowWindow(hwnd_window, SW_SHOWDEFAULT);
  UpdateWindow(hwnd_window);

  SendMessageW(hwnd_progress_bar, PBM_SETRANGE, 0, MAKEWPARAM(0, 100));
  SendMessageW(hwnd_progress_bar, PBM_SETSTEP, (WPARAM) 1, 0);
}


LONG remove_directory(const char* wzDirectory)
{
    char szDir[MAX_PATH+1];  // +1 for the double null terminate
    SHFILEOPSTRUCT fos = {0};

    strncpy(szDir,wzDirectory,MAX_PATH);
    int len = strlen(szDir);
    szDir[len+1] = 0; // double null terminate for SHFileOperation
    // delete the folder and everything inside
    fos.wFunc = FO_DELETE;
    fos.pFrom = szDir;
    fos.fFlags = FOF_SILENT | FOF_NOCONFIRMATION |FOF_NOERRORUI | FOF_NOCONFIRMMKDIR;
    return SHFileOperationA( &fos );
}


char DLL_EXPORT make_clone()
{
    progress_data pd = {{0}};
	git_repository *cloned_repo = NULL;
	git_clone_options clone_opts = GIT_CLONE_OPTIONS_INIT;
	git_checkout_opts checkout_opts = GIT_CHECKOUT_OPTS_INIT;

    int error=0;

	checkout_opts.checkout_strategy = GIT_CHECKOUT_SAFE_CREATE;
	checkout_opts.progress_cb = checkout_progress;
	checkout_opts.progress_payload = &pd;
	clone_opts.checkout_opts = checkout_opts;
	clone_opts.remote_callbacks.transfer_progress = &fetch_progress;
	clone_opts.remote_callbacks.payload = &pd;
	clone_opts.ignore_cert_errors = 1;
    make_window();
    stringstream _sstr; _sstr << get_path_to_rep() <<"\\"<<PRJ_BIN_FOLDER;
	error = git_clone(&cloned_repo, GIT_REPOSITORY_URL,_sstr.str().c_str(), &clone_opts);
	if (error == 0)
        {
        if (cloned_repo) git_repository_free(cloned_repo);
        }
    else
        {
        MessageBox(NULL, TEXT("������ ������������ ����������� git_clone()"), TEXT("������"), MB_ICONERROR | MB_OK);
        return error;
        }
    return error;
}

char _is_update_needed(char* remote_commit_id)
{
  CURL *curl;
  CURLcode res;
  struct MemoryStruct chunk;
  chunk.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */
  chunk.size = 0;    /* no data at this point */

  curl_global_init(CURL_GLOBAL_DEFAULT);
  curl = curl_easy_init();
  if(curl)
        {
        curl_easy_setopt(curl, CURLOPT_URL,GIT_BITBUCKET_API_CHANGESET_URL);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, get_version_file_cb);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);
        curl_easy_setopt(curl, CURLOPT_SSL_SESSIONID_CACHE, 0);
#ifdef SKIP_PEER_VERIFICATION
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
#endif
#ifdef SKIP_HOSTNAME_VERIFICATION
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
#endif
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        }
        if(res)
            {
            //MessageBoxA(NULL, TEXT("������ ��������� �����-������."), TEXT("������"), MB_ICONERROR | MB_OK);
            remote_commit_id = NULL;
            free(chunk.memory);
            curl_global_cleanup();
            return UPDATE_ERROR;
            }
        else
            {
            Json::Value root;
            Json::Reader reader;
            bool parsedSuccess = reader.parse(chunk.memory,
                                               root,
                                               false);
            if(not parsedSuccess)
                {
                MessageBoxA(NULL, TEXT("������ Bitbucket API"), TEXT("������"), MB_ICONERROR | MB_OK);
                remote_commit_id = NULL;
                free(chunk.memory);
                curl_global_cleanup();
                return UPDATE_ERROR;
                }

           const Json::Value error = root["error"];
           if(error.size())
                {
                MessageBoxA(NULL, TEXT("������ ����������� ����������"), TEXT("������"), MB_ICONERROR | MB_OK);
               //MessageBoxA(NULL, (LPCSTR)error.get("message","UTF-8").asString().c_str(), TEXT("������"), MB_ICONERROR | MB_OK);
                remote_commit_id = NULL;
                free(chunk.memory);
                curl_global_cleanup();
                return UPDATE_ERROR;
                }

           uint32_t index = 0;
           const Json::Value changeset = root["changesets"][index];
           if(changeset.size())
                {
                char remote_last_commit[41] = {0};
                stringstream _sstr; _sstr << get_path_to_rep() <<"\\"<<VER_FILE_NAME;
                strncpy(remote_last_commit,changeset.get("raw_node","UTF-8").asCString(),40);

                FILE* ver_file_hnd = fopen(_sstr.str().c_str(),"r+");
                if(ver_file_hnd)
                    {
                    char local_last_commit[41] = {0};
                    fgets(local_last_commit,41,ver_file_hnd);
                    if(strncmp(remote_last_commit,local_last_commit,40))
                        {
                        strncpy(remote_commit_id,remote_last_commit,40);
                        free(chunk.memory);
                        curl_global_cleanup();
                        return UPDATE_NEEDED;   //make query to user
                        }
                    else
                        {
                        strncpy(remote_commit_id,remote_last_commit,40);
                        free(chunk.memory);
                        curl_global_cleanup();
                        return UPDATE_NOT_NEEDED;   //make query to user
                        }
                    }
                else
                    {
                    strncpy(remote_commit_id,remote_last_commit,40);
                    free(chunk.memory);
                    curl_global_cleanup();
                    return UPDATE_NEEDED;   //make query to user
                    }
                }
            else
                {
                remote_commit_id = NULL;
                MessageBoxA(NULL, TEXT("������ Bitbucket API"), TEXT("������"), MB_ICONERROR | MB_OK);
                free(chunk.memory);
                curl_global_cleanup();
                return UPDATE_ERROR;
                }
//end json parse
        }
  remote_commit_id = NULL;
  free(chunk.memory);
  curl_global_cleanup();
  return res;
}

char _make_update(update_params params)
{

    char remote_last_commit[41]={0};

    stringstream _sstr; _sstr << get_path_to_rep() <<"\\"<<PRJ_BIN_FOLDER;
            //MessageBox(NULL,_sstr.str().c_str(), TEXT("info"), MB_ICONERROR | MB_OK);

    if(params==UPDATE_FORCE)
        {
        remove_directory(_sstr.str().c_str());
        if(!make_clone())
            {
            stringstream ver_file_path; ver_file_path<<get_path_to_rep()<<"\\"<<VER_FILE_NAME;
            FILE* ver_file_hnd = fopen(ver_file_path.str().c_str(),"w+");
            fseek ( ver_file_hnd , 0 , SEEK_SET );
            _is_update_needed(remote_last_commit);
            fputs(remote_last_commit,ver_file_hnd);
            if((params==UPDATE_FORCE)||(params==UPDATE_STANDALONE))
                 {
                 MessageBoxA(NULL, TEXT("���������� ����������, ������������� ���������"), TEXT("����������"), MB_ICONINFORMATION | MB_OK);
                 }
            fclose(ver_file_hnd);
            return UPDATE_SUCCESSFUL;
            }
        else
            {
            return UPDATE_ERROR;
            }
        }

    int is_update_needed_error = _is_update_needed(remote_last_commit);
    if((!is_update_needed_error))
        {
        int query_ans = MessageBoxA(NULL,TEXT("�������� ���������?"),TEXT("����������"),MB_ICONQUESTION | MB_YESNO);
        if (query_ans == IDYES)
            {
            int error = remove_directory(_sstr.str().c_str());
            if((error)&&(params==UPDATE_AUTO))
                {
                MessageBox(NULL, TEXT("������ �������� ������� �����������. �������� �������� ��������"), TEXT("������"), MB_ICONERROR | MB_OK);
                return UPDATE_ERROR;
                }
            else
                {
                if(!make_clone())
                    {
                    stringstream ver_file_path; ver_file_path<<get_path_to_rep()<<"\\"<<VER_FILE_NAME;
                    FILE* ver_file_hnd = fopen(ver_file_path.str().c_str(),"w+");

                    fseek ( ver_file_hnd , 0 , SEEK_SET );
                    fputs(remote_last_commit,ver_file_hnd);
                    if((params==UPDATE_FORCE)||(params==UPDATE_STANDALONE))
                        {
                        MessageBoxA(NULL, TEXT("���������� ����������, ������������� ���������"), TEXT("����������"), MB_ICONINFORMATION | MB_OK);
                        }
                    fclose(ver_file_hnd);
                    return UPDATE_SUCCESSFUL;
                    }
                }
            }
        else
            {
            if((params==UPDATE_FORCE)||(params==UPDATE_STANDALONE))
                {
                MessageBoxA(NULL, TEXT("���������� ��������"), TEXT("����������"), MB_ICONEXCLAMATION | MB_OK);
                return UPDATE_ERROR;
                }
            }
        }
    return UPDATE_NOT_NEEDED;
}

unsigned int DLL_EXPORT is_update_needed()
{
    char buf[41] = {0};
    return _is_update_needed(buf);
}

extern "C" DLL_EXPORT int make_update_auto()
{
    return _make_update(UPDATE_AUTO);
}

extern "C" DLL_EXPORT int make_update_standalone()
{
    return _make_update(UPDATE_STANDALONE);
}

extern "C" DLL_EXPORT int make_update_force()
{
    return _make_update(UPDATE_FORCE);
}


extern "C" DLL_EXPORT BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    switch (fdwReason)
    {
        case DLL_PROCESS_ATTACH:
            // attach to process
            // return FALSE to fail DLL load
            break;

        case DLL_PROCESS_DETACH:
            // detach from process
            break;

        case DLL_THREAD_ATTACH:
            // attach to thread
            break;

        case DLL_THREAD_DETACH:
            // detach from thread
            break;
    }
    return TRUE; // succesful
}
